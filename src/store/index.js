import Vue from "vue";
import Vuex from "vuex";

import state from "./state";
import mutations from "./mutations";
import actions from "./actions";

import constante from "./modules/constantes";
import departamento from "./modules/departamentos";
import municipio from "./modules/municipios";
import pagador from "./modules/pagadores";
import prestador from "./modules/prestadores";
import persona from "./modules/personas";
import autenticacion from "./modules/autenticacion";

Vue.use(Vuex);

const store = new Vuex.Store({
  state,
  mutations,
  actions,
  modules: {
    constante,
    departamento,
    municipio,
    pagador,
    prestador,
    persona,
    autenticacion
  }
});

export default store;
