export const showError = function(context, error) {
  let dispatch = context.dispatch;

  console.error(error);

  const snackbar = {
    color: "red",
    text: "No se puedo establecer conexión con la base de datos" + "\n" + error
  };

  dispatch("showAppSnackBar", snackbar, { root: true });
};
