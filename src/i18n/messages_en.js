export default {
  en: {
    placeholder: {
      filterdata: "Filter data"
    },

    tooltip: {
      refresh: "Refresh"
    },

    text: {
      code: "Code",
      name: "Name"
    }
  }
};
