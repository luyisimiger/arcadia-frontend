import _ from "lodash";
import api from "@/services/api";
import PagadorService from "./PagadorService";
import PrestadorService from "./PrestadorService";
import PersonaService from "./PersonaService";

const path = "/remisiones";

const remision = {
  id: 0,
  codigo: "",
  estado: 1,
  fechahora: undefined,
  info_clinica: "",
  prestador: PrestadorService.default_object(),
  paciente: {
    direccion: "",
    telefono: "",
    persona: PersonaService.default_object()
  },
  acudiente: {
    direccion: "",
    telefono: "",
    persona: PersonaService.default_object()
  },
  profesional: {
    telefono: "",
    persona: PersonaService.default_object()
  },
  pagador: PagadorService.default_object(),
  servicio_referencia: "",
  servicio_referente: ""
};

export default {
  default_object() {
    return _.cloneDeep(remision);
  },

  fetch() {
    return api.get(path);
  },

  fetch_presentadas(idprestador) {
    let url = `${path}/presentadas/${idprestador}`;
    return api.get(url);
  },

  fetchone(id) {
    let url = `${path}/${id}`;
    return api.get(url);
  },

  actualizarBitacora({ idremision, descripcion }) {
    let url = `${path}/actualizar/bitacora`;
    return api.post(url, { idremision, descripcion });
  },

  solicitar({ remision }) {
    let url = `${path}/solicitar`;
    return api.post(url, { remision });
  },

  recepcionar(id) {
    let url = `${path}/${id}/recepcionar`;
    return api.get(url);
  },

  presentar({ idremision, idprestadores }) {
    let url = `${path}/presentar`;
    return api.post(url, { idremision, idprestadores });
  },

  aceptar({ idremision, idprestador }) {
    let url = `${path}/aceptar`;
    return api.post(url, { idremision, idprestador });
  },

  aceptar_multiple({ idremision, idprestadores }) {
    let url = `${path}/aceptar/multiple`;
    return api.post(url, { idremision, idprestadores });
  },

  recibir({ idremision, idprestador }) {
    let url = `${path}/recibir`;
    return api.post(url, { idremision, idprestador });
  },

  rechazar({ idremision, idprestador }) {
    let url = `${path}/rechazar`;
    return api.post(url, { idremision, idprestador });
  },

  aceptar_id(id) {
    let url = `${path}/${id}/aceptar`;
    return api.get(url);
  },

  recibir_id(id) {
    let url = `${path}/${id}/recibir`;
    return api.get(url);
  }
};
