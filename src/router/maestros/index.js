import personas from "./personas";

export default [
  ...personas,
  {
    path: "/maestros/prestadores",
    meta: { breadcrumb: true },
    name: "Prestador",
    component: () => import(`@/views/maestros/Prestador.vue`)
  },
  {
    path: "/maestros/pagadores",
    meta: { breadcrumb: true },
    name: "Pagador",
    component: () => import(`@/views/maestros/Pagador.vue`)
  },
  {
    path: "/maestros/municipios",
    meta: { breadcrumb: true },
    name: "Municipio",
    component: () => import(`@/views/maestros/Municipio.vue`)
  },
  {
    path: "/maestros/departamentos",
    meta: { breadcrumb: true },
    name: "departamento",
    component: () => import(`@/views/maestros/Departamento.vue`)
  }
];
