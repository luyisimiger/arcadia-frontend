export default {
  appsnackbar: {
    timeout: 6000,
    show: false,
    text: "",
    color: ""
  },
  applanguage: "es",
  pageheader: {
    refresh: false
  }
};
