import vuetify from "./vuetify";
import i18n from "./i18n";
import vuemoment from "./vuemoment";

export default { vuetify, i18n, vuemoment };
