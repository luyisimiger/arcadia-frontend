import * as types from "./mutations_types/root";

export default {
  [types.SET_APPSNACKBAR](state, snackbar) {
    Object.assign(state.appsnackbar, snackbar);
    state.appsnackbar.show = true;
  },

  [types.SET_APPLANGUAGE](state, value) {
    state.applanguage = value;
  },

  [types.SET_PAGEHEADER_REFRESH](state, value) {
    state.pageheader.refresh = value;
  }
};
