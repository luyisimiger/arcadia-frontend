export default [
  { header: "Remisiones" },
  {
    title: "Solicitar Remisión",
    group: "apps",
    icon: "mdi-clipboard-pulse",
    name: "remision-new"
  },
  {
    title: "Remisiones",
    group: "apps",
    icon: "mdi-clipboard-pulse",
    name: "remision-list"
  }/*,
  {
    title: "Remisiones Presentadas",
    group: "apps",
    icon: "mdi-clipboard-pulse",
    name: "remision-list-no-propias"
  }*/
];
