import others from "./other.routes";
import maestros from "./maestros/index";
import remisiones from "./remisiones";

export default [...maestros, ...remisiones, ...others];
