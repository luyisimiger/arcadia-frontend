import Service from "@/services/MunicipioService";
import * as types from "../mutations_types/municipios";
import { showError } from "@/util/vuex";

/* json data example
[
  {
    "codigo_departamento": "13",
    "codigo": "13001",
    "nombre": "Cartagena",
    "latitud": 10.4269427214,
    "longitud": -75.4759895798
  }
]
*/

// initial state
const state = {
  municipios: []
};

// getters
const getters = {
  filtrarPorDepartamento: (state, depart) => {
    return state.municipios.filter(m => m.departamento.codigo == depart);
  }
};

// actions
const actions = {
  fetch: function(context) {
    let commit = context.commit;
    let promesa = Service.getMunicipios();

    promesa
      .then(function(response) {
        var objectlist = response.data;
        commit(types.SET_MUNICIPIOS, objectlist);
      })
      .catch(function(error) {
        console.log("error in action: municipios/fetch");
        commit(types.SET_MUNICIPIOS, []);
        showError(context, error);
      });

    return promesa;
  }
};

// mutations
const mutations = {
  [types.SET_MUNICIPIOS]: function(state, objectlist) {
    state.municipios = objectlist;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
