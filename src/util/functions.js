export const quitarTildes = function(text) {
  text = text.replace("á", "a");
  text = text.replace("é", "e");
  text = text.replace("í", "i");
  text = text.replace("ó", "o");
  text = text.replace("ú", "u");

  text = text.replace("Á", "A");
  text = text.replace("É", "E");
  text = text.replace("Í", "I");
  text = text.replace("Ó", "O");
  text = text.replace("Ú", "U");

  return text;
};

export const getRandomInt = function(min, max) {
  return Math.floor(Math.random() * (max + 1 - min)) + min;
};

export const auth_token = () => localStorage.auth_token || "";
