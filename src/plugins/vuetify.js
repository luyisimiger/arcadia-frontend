import Vue from "vue";
import Vuetify from "vuetify/lib";
import "vuetify/src/stylus/app.styl";
import "../theme/default.styl";

// Icons Font for vuetify
import "material-design-icons-iconfont/dist/material-design-icons.css";
import "@mdi/font/css/materialdesignicons.css";
import "font-awesome/css/font-awesome.css";

// Translation provided by Vuetify (javascript)
import en from "vuetify/es5/locale/en";
import es from "vuetify/es5/locale/es";

import mdi_icons from "@/data/icons/mdi";

var i18nConfig = {
  locales: { en, es },
  current: "es"
};

Vue.use(Vuetify, {
  theme: {
    primary: "#ee44aa",
    secondary: "#424242",
    accent: "#82B1FF",
    error: "#FF5252",
    info: "#2196F3",
    success: "#4CAF50",
    warning: "#FFC107"
  },
  customProperties: true,
  lang: {
    ...i18nConfig
  },
  iconfont: "mdi",
  icons: mdi_icons
});

export default {};
