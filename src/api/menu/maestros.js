export default [
  { header: "Entidades" },
  {
    title: "Personas",
    group: "apps",
    icon: "people",
    name: "Persona"
  },
  {
    title: "Prestadores",
    group: "apps",
    icon: "mdi-stethoscope",
    name: "Prestador"
  },
  {
    title: "Pagadores",
    group: "apps",
    icon: "mdi-bank",
    name: "Pagador"
  },
  {
    title: "Municipios",
    group: "apps",
    icon: "mdi-map-marker",
    name: "Municipio"
  },
  {
    title: "Departamentos",
    group: "apps",
    icon: "mdi-map",
    name: "departamento"
  }
];
