export default {
  es: {
    placeholder: {
      filterdata: "Filtrar datos"
    },

    tooltip: {
      refresh: "Actualizar"
    },

    text: {
      code: "Código",
      name: "Nombre"
    }
  }
};
