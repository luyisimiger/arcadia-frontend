import Service from "@/services/PrestadorService";
import * as types from "../mutations_types/prestadores";

/* json data example
[
  {
    "codigo": "7000101378",
    "sigla": "",
    "nombre": "FUNDACION MARIA REINA",
    "direccion": "calle27# 18- 50",
    "telefono": "2824866",
    "email": "info@fundacionmariareina.com",
    "url": ""
  }
]
*/

// initial state
const state = {
  prestadores: []
};

// getters
const getters = {};

// actions
const actions = {
  fetch: function({ commit, dispatch }) {
    Service.getPrestadores()
      .then(function(response) {
        console.log("execute fetch Prestadores");

        var objectlist = response.data;
        commit(types.SET_PRESTADORES, objectlist);
      })
      .catch(function(error) {
        console.log("error in action: prestadores/fetch");
        console.log(error);

        const snackbar = {
          color: "red",
          text: "No se puedo establecer conexión con la base de datos"
        };

        commit(types.SET_PRESTADORES, []);
        dispatch("showAppSnackBar", snackbar, { root: true });
      });
  }
};

// mutations
const mutations = {
  [types.SET_PRESTADORES]: function(state, objectlist) {
    state.prestadores = objectlist;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
