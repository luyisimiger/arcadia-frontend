import maestros from "./maestros";
import remisiones from "./remisiones";

const Menu = [...maestros, ...remisiones];

export default Menu;
