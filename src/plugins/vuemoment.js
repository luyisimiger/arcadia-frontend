import Vue from "vue";
import vuemoment from "vue-moment";
import moment from "moment-timezone";
import "moment/locale/es";

moment.tz.setDefault("America/Bogota");

Vue.use(vuemoment, {
  moment
});

export default {};
