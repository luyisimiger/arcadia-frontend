// initial state
const state = {
  tipoIdentificacion: [
    { valor: "CC", nombre: "Cedula de ciudadania" },
    { valor: "TI", nombre: "Tarjeta de identificacion" },
    { valor: "PS", nombre: "Pasaporte" }
  ],

  sexo: [
    { valor: "F", nombre: "Femenino" },
    { valor: "M", nombre: "Masculino" }
  ]
};

// getters
const getters = {};

// actions
const actions = {};

// mutations
const mutations = {};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
