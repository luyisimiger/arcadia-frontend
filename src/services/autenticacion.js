import api from "@/services/api";

export default {
  register(email, password) {
    return api.post("/auth/register", { email, password });
  },

  login(email, password) {
    return api.post("/auth/login", { email, password });
  },

  logout() {
    return api.post("/auth/logout");
  }
};
