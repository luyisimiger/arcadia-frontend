import NProgress from "nprogress";
import store from "@/store/index";

export default [
  {
    path: "*",
    meta: {
      public: true
    },
    redirect: {
      path: "/404"
    }
  },
  {
    path: "/404",
    meta: {
      public: true
    },
    name: "NotFound",
    component: () =>
      import(/* webpackChunkName: "routes" */
      `@/views/NotFound.vue`)
  },
  {
    path: "/403",
    meta: {
      public: true
    },
    name: "AccessDenied",
    component: () =>
      import(/* webpackChunkName: "routes" */
      `@/views/Deny.vue`)
  },
  {
    path: "/500",
    meta: {
      public: true
    },
    name: "ServerError",
    component: () =>
      import(/* webpackChunkName: "routes" */
      `@/views/Error.vue`)
  },
  {
    path: "/login",
    meta: {
      public: true
    },
    name: "Login",
    component: () =>
      import(/* webpackChunkName: "routes" */
      `@/views/Login.vue`),

    beforeEnter(to, from, next) {
      let user_autenticated = store.getters["autenticacion/user_autenticated"];
      if (user_autenticated) {
        NProgress.done();
        next(false);
      } else next();
    }
  },
  {
    path: "/",
    meta: {},
    name: "Root",
    redirect: {
      name: "remision-list"
    }
  }
];
