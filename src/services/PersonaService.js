import _ from "lodash";
import api from "@/services/api";

const path = "/personas";

const persona = {
  tipoIdentificacion: "",
  numeroIdentificacion: "",
  fechaNacimiento: "",
  sexo: "",
  apellido1: "",
  apellido2: "",
  nombre1: "",
  nombre2: ""
};

export default {
  default_object() {
    return _.cloneDeep(persona);
  },

  fetch() {
    return api.get(path);
  },

  create(persona) {
    return api.post(path, persona);
  },

  read(id) {
    let xpath = path + "/" + id;
    return api.get(xpath);
  },

  update(persona) {
    let xpath = path + "/" + persona.id;
    return api.put(xpath, persona);
  },

  delete(persona) {
    let xpath = path + "/" + persona.id;
    return api.delete(xpath);
  }
};
