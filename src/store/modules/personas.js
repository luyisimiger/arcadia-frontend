import Service from "@/services/PersonaService";
import * as types from "../mutations_types/personas";
import { showError } from "@/util/vuex";

/* json data example
[
  {
    "apellido1": "ALVIS",
    "apellido2": "CARDOZO",
    "nombre1": "YOSHIRA",
    "nombre2": "INES",
    "tipoIdentificacion": "CC",
    "numeroIdentificacion": "1128046080",
    "fechaNacimiento": "2/23/1986"
  }
]
*/

// initial state
const state = {
  personas: [],
  //FIXME: borrar este estado
  updateFlag: false
};

// getters
const getters = {
  getPersona: state => id => {
    return state.personas.find(p => p.id === id);
  }
};

// actions
const actions = {
  fetch: function(context) {
    let commit = context.commit;
    let promesa = Service.fetch();

    promesa
      .then(function(response) {
        var objectlist = parseData(response.data);
        commit(types.SET_PERSONAS, objectlist);
      })
      .catch(function(error) {
        commit(types.SET_PERSONAS, []);
        showError(context, error);
      });

    return promesa;
  },

  create(context, persona) {
    let dispatch = context.dispatch;
    let promesa = Service.create(persona);

    promesa
      .then(() => {
        dispatch("fetch");
      })
      .catch(error => {
        console.log("error in action: personas/create");
        showError(context, error);
      });

    return promesa;
  },

  update(context, payload) {
    let dispatch = context.dispatch;
    let promesa = Service.update(payload.persona);

    promesa
      .then(() => {
        dispatch("fetch");
      })
      .catch(error => {
        console.log("error in action: personas/update");
        showError(context, error);
      });

    return promesa;
  },

  delete(context, persona) {
    let promesa = Service.delete(persona);

    promesa.catch(error => {
      console.log("error in action: personas/delete");
      showError(context, error);
    });

    return promesa;
  }
};

// mutations
const mutations = {
  [types.SET_PERSONAS]: function(state, objectlist) {
    state.personas = objectlist;
  }
};

// helpers functions
function parseData(personas) {
  return personas;
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
