export default [
  {
    path: "/remisiones/solicitar",
    meta: { breadcrumb: true },
    name: "remision-new",
    component: () => import(`@/views/remisiones/SolicitarRemision.vue`)
  },
  {
    path: "/remisiones",
    meta: { breadcrumb: true, propias: true },
    name: "remision-list",
    component: () => import(`@/views/remisiones/ListarRemisiones.vue`)
  },
  {
    path: "/remisiones/:idremision/detalle",
    meta: { breadcrumb: false },
    name: "remision-detail",
    component: () => import(`@/views/remisiones/DetalleRemision.vue`),
    props: true
  }
];
