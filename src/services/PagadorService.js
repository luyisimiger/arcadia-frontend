import _ from "lodash";
import api from "@/services/api";

const path = "/pagadores";

const pagador = {
  id: 0,
  codigo: "",
  nombre: "",
  correo_referencia: "",
  direccion: "",
  sigla: "",
  tipo: "",
  url: ""
};

export default {
  default_object() {
    return _.cloneDeep(pagador);
  },

  getPagadores() {
    return api.get(path);
  },

  buscar(options) {
    let url = `${path}/seleccionar`;
    return api.post(url, options);
  }
};
