import "roboto-fontface/css/roboto/roboto-fontface.css";
import "@mdi/font/css/materialdesignicons.css";

import Vue from "vue";
import plugins from "./plugins/index";
import App from "./App.vue";
import router from "./router/";
import store from "./store/index";
import "./registerServiceWorker";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n: plugins.i18n,
  render: h => h(App)
}).$mount("#app");
