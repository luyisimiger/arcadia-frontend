import _ from "lodash";
import api from "@/services/api";

const path = "/departamentos";

const departamento = {
  id: 0,
  codigo: "",
  nombre: ""
};

export default {
  default_object() {
    return _.cloneDeep(departamento);
  },

  getDepartamentos() {
    return api.get(path);
  }
};
