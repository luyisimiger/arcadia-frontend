module.exports = {
  publicPath: "",
  //publicPath: "/" + process.env.VUE_APP_PUBLICPATH + "/",
  //publicPath: "/proyecto.tesis/",

  devServer: {
    proxy: {
      "/api*": {
        target: "http://127.0.0.1:5000/"
      }
    }
  }
};
