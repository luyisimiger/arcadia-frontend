export const RemisionEstado = {
  SOLICITADA: 1,
  DEVUELTA: 2,
  PENDIENTE: 3,
  PRESENTADA: 4,
  ACEPTADA: 5,
  SUSPENDIDA: 6,
  RECIBIDA: 7
};

Object.freeze(RemisionEstado);
