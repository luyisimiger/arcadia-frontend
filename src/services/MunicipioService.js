import _ from "lodash";
import api from "@/services/api";
import DepartamentoService from "./DepartamentoService";

const path = "/municipios";

const municipio = {
  id: 0,
  codigo: "",
  nombre: "",
  departamento: DepartamentoService.default_object()
};

export default {
  default_object() {
    return _.cloneDeep(municipio);
  },

  getMunicipios() {
    return api.get(path);
  }
};
