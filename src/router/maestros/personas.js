export default [
  {
    path: "/maestros/personas",
    name: "Persona",
    redirect: {
      name: "persona-list",
      params: { page: 1 }
    }
  },
  {
    path: "/maestros/personas/p/:page",
    meta: { breadcrumb: true },
    name: "persona-list",
    props: true,
    component: () => import(`@/views/maestros/Persona.vue`)
  },
  {
    path: "/maestros/personas/new",
    name: "persona-new",
    component: () => import(`@/views/maestros/personas/PersonaNew.vue`),
    props: true
  },
  {
    path: "/maestros/personas/:id/edit",
    name: "persona-edit",
    component: () => import(`@/views/maestros/PersonaDetail.vue`),
    props: true
  }
];
