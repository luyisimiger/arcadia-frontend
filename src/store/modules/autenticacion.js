import service from "@/services/autenticacion";
import router from "@/router";
import { auth_token } from "@/util/functions";

/* json data example
[
  {
    'user_id': 1,
    'email': "admin@gmail.com",
    'admin': True,
    'registered_on': "20200314 05:05:10"
  }
]
*/

// initial state
const state = {
  auth_token: auth_token(),
  usuario: null,
  status: "",
  message: "",
  profile: {
    prestador: {
      id: 823
    }
  }
};

// getters
const getters = {
  is_admin: state => {
    return state.usuario.admin;
  },

  user_autenticated: state => {
    return state.auth_token == "" ? false : true;
  }
};

// actions
const actions = {
  login(context, { email, password }) {
    let commit = context.commit;
    let promesa = service.login(email, password);

    promesa
      .then(response => {
        let status = response.data.status;
        let message = response.data.message;

        commit("SET_TOKEN", response.data.auth_token);
        commit("SET_ALERT", { status, message });
        router.push("/");
      })
      .catch(err => {
        console.log(err);
        let status = err.response.data.status;
        let message = err.response.data.message;

        commit("SET_TOKEN", "");
        commit("SET_ALERT", { status, message });
        console.log("error in action: autenticacion/login");
      });

    return promesa;
  },

  logout(context) {
    let state = context.commit;
    let commit = context.commit;
    let promesa = service.logout(state.auth_token);

    promesa
      .then(() => {
        commit("SET_TOKEN", "");
        commit("SET_ALERT", { status: "", message: "" });
      })
      .catch(() => {
        console.log("error in action: autenticacion/logout");
      });

    return promesa;
  }
};

// mutations
const mutations = {
  SET_TOKEN: function(state, auth_token) {
    state.auth_token = auth_token;
    localStorage.auth_token = auth_token;
  },

  SET_ALERT(state, { status, message }) {
    state.status = status;
    state.message = message;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
