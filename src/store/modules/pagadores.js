import Service from "@/services/PagadorService";
import * as types from "../mutations_types/pagadores";

/* json data example
[
  {
    "codigo": "EPS005",
    "sigla": "SANITAS S.A. EPS",
    "nombre": "SANITAS S.A. EPS",
    "direccion": "CL 100 11B 67 Piso 4A",
    "telefono": "6466060 ext 10101",
    "email": "autorisanitas@colsanitas.com",
    "url": "http://www.epssanitas.com"
  }
]
*/

// initial state
const state = {
  pagadores: []
};

// getters
const getters = {};

// actions
const actions = {
  fetch: function({ commit, dispatch }) {
    Service.getPagadores()
      .then(function(response) {
        console.log("execute fetch Pagadores");

        var objectlist = response.data;
        commit(types.SET_PAGADORES, objectlist);
      })
      .catch(function(error) {
        console.log("error in action: pagadores/fetch");
        console.log(error);

        const snackbar = {
          color: "red",
          text: "No se puedo establecer conexión con la base de datos"
        };

        commit(types.SET_PAGADORES, []);
        dispatch("showAppSnackBar", snackbar, { root: true });
      });
  }
};

// mutations
const mutations = {
  [types.SET_PAGADORES]: function(state, objectlist) {
    state.pagadores = objectlist;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
