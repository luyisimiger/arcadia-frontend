const tagsToReplace = {
  "&": "&amp;",
  "<": "&lt;",
  ">": "&gt;"
};

function escapeHTML(str) {
  return str.replace(/[&<>]/g, tag => tagsToReplace[tag] || tag);
}

function getMaskedCharacters(query, text) {
  const searchInput = (query || "").toString().toLocaleLowerCase();
  const index = text.toLocaleLowerCase().indexOf(searchInput);

  if (index < 0) return { start: text, middle: "", end: "" };

  const start = text.slice(0, index);
  const middle = text.slice(index, index + searchInput.length);
  const end = text.slice(index + searchInput.length);
  return { start, middle, end };
}

function genHighlight(text) {
  return `<span class="v-list__tile__mask">${escapeHTML(text)}</span>`;
}

export function genFilteredText(query, text) {
  text = (text || "").toString();

  if (!query) return escapeHTML(text);

  const { start, middle, end } = getMaskedCharacters(query, text);

  return `${escapeHTML(start)}${genHighlight(middle)}${escapeHTML(end)}`;
}

export default {};
