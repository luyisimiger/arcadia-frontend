import messages_en from "./messages_en";
import messages_es from "./messages_es";

export default {
  ...messages_en,
  ...messages_es
};
