import * as types from "./mutations_types/root";

export default {
  initApp({ dispatch }) {
    dispatch("departamento/fetchDepartamentos").then(() => {
      dispatch("municipio/fetch");
    });
    /*
    dispatch("pagador/fetch");
    dispatch("prestador/fetch");
    dispatch("persona/fetch");
    */
  },

  showAppSnackBar({ commit }, snackbar) {
    commit(types.SET_APPSNACKBAR, snackbar);
  },

  startRefresh({ commit }) {
    commit(types.SET_PAGEHEADER_REFRESH, true);
  },

  finishRefresh({ commit }) {
    commit(types.SET_PAGEHEADER_REFRESH, false);
  }
};
