import DepartamentoService from "@/services/DepartamentoService";
import * as types from "../mutations_types/departamentos";

/* json data example
[
  {
    "codigo": "13",
    "nombre": "Bolívar",
    "latitud": 9,
    "longitud": -74.3333333
  }
]
*/

// initial state
const state = {
  departamentos: []
};

// getters
const getters = {
  departamentosOrdenados(state) {
    return state.departamentos.sort((a, b) => {
      var r = 0;

      if (a.nombre < b.nombre) r = -1;
      else if (a.nombre > b.nombre) r = 1;
      return r;
    });
  }
};

// actions
const actions = {
  fetchDepartamentos: function(context) {
    DepartamentoService.getDepartamentos()
      .then(function(response) {
        console.log("execute fetch Departamentos");

        var objectlist = response.data;
        context.commit(types.SET_DEPARTAMENTOS, objectlist);
      })
      .catch(function(error) {
        console.log("error in action: fetchDepartamentos");
        console.log(error);

        const snackbar = {
          color: "red",
          text: "No se puedo establecer conexión con la base de datos"
        };

        context.commit(types.SET_DEPARTAMENTOS, []);
        context.dispatch("showAppSnackBar", snackbar, { root: true });
      });
  }
};

// mutations
const mutations = {
  [types.SET_DEPARTAMENTOS]: function(state, objectlist) {
    state.departamentos = objectlist;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
