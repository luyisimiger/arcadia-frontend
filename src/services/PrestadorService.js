import _ from "lodash";
import api from "@/services/api";
import MunicipioService from "./MunicipioService";

const path = "/prestadores";

const prestador = {
  id: 0,
  codigo: "",
  nombre: "",
  municipio: MunicipioService.default_object()
};

export default {
  default_object() {
    return _.cloneDeep(prestador);
  },

  getPrestadores(params) {
    return api.get(path, { params: params });
  },

  buscar(options) {
    let url = `${path}/seleccionar`;
    return api.post(url, options);
  }
};
